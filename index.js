// set up dependencies
const express = require("express")

// server setup
const app = express()
const port = 5000;

//middle ware para maread as json yung mga isesend F
app.use(express.json())

const newUser = {
    firstName:'levi',
    lastName:'ackerman',
    age: 28,
    contactNo:'09123456789',
    batchNo: 166,
    email:'leviAckerman@mail.com',
    password:'thequickbrownfoxjumpsoverthelazydog'
}


module.exports = {
    newUser :newUser
}

// Route Connection
// const userRoutes = require("./routes/user")
// end of Route connection


// set route endpoint
// app.use("/api/users", userRoutes)
// end of route endpoint


app.listen(port, () => console.log(`Now listening to port ${port}`))