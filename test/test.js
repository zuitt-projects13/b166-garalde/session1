const { assert } = require('chai');
const { newUser } = require('../index.js')


// name which is String, function
// describe will handle all test cases
describe('Test User Object', () => {
    // it certifiies certain test case
    it('Assert newUser type is an object', () => {
        // equal(what is compared, what is expected)
        // typeof returns string
        assert.equal(typeof(newUser),'object')
    })

    it('Assert newUser email is type string', () => {
        assert.equal(typeof(newUser.email),'string')
    })

    it('Assert that newUser email is not undefined', () => {
        assert.notEqual(typeof(newUser.email),'undefined')
    })

    it('Assert newUser password is type string', () => {
        assert.equal(typeof(newUser.password),'string')
    })

    it('Assert newUser password has atleast 16 characters', () => {
        assert.isAtLeast(newUser.password.length,16)
    })    
})

describe('Activity Test', () => {

    it('Assert newUser firstName type is a string', () => {
        assert.equal(typeof(newUser.firstName),'string')
    })

    it('Assert newUser lastName type is a string', () => {
        assert.equal(typeof(newUser.lastName),'string')
    })

    it('Assert that newUser firstName is not undefined', () => {
        assert.notEqual(typeof(newUser.firstName),'undefined')
    })

    it('Assert that newUser lastName is not undefined', () => {
        assert.notEqual(typeof(newUser.lastName),'undefined')
    })

    it('Assert newUser age is atleast 18', () => {
        assert.isAtLeast(newUser.age,18)
    }) 

    it('Assert newUser age type ia a number', () => {
        assert.equal(typeof(newUser.age),'number')
    })

    it('Assert newUser contact number type is a string', () => {
        assert.equal(typeof(newUser.contactNo),'string')
    })

    it('Assert newUser batch number type is a string', () => {
        assert.equal(typeof(newUser.contactNo),'string')
    })

    it('Assert that newUser batch number  is not undefined', () => {
        assert.notEqual(typeof(newUser.batchNo),'undefined')
    })

})
